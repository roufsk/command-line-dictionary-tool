var methods = {
    getUrl: function(choice, input, apiKey, limit) {
        var mainUrl = "http://api.wordnik.com:80/v4/word.json/";
        var urlObject = {
            1: mainUrl + input + "/definitions?limit=200&includeRelated=true&useCanonical=false&includeTags=false",
            2: mainUrl + input + "/relatedWords?useCanonical=false&limitPerRelationshipType=" + limit,
            3: mainUrl + input + "/relatedWords?useCanonical=false&limitPerRelationshipType=" + limit,
            4: mainUrl + input + "/examples?includeDuplicates=false&useCanonical=false&skip=0",
            6: "http://api.wordnik.com:80/v4/words.json/wordOfTheDay?date=" + input,
            7: "http://api.wordnik.com:80/v4/words.json/randomWord?hasDictionaryDef=false&minCorpusCount=0&maxCorpusCount=-1&minDictionaryCount=1&maxDictionaryCount=-1&minLength=5&maxLength=-1"
        };
        return urlObject[choice] + apiKey;
    },
    processParsedBody: function(choice, word, parsedBody, isGame) {
        if (!(isGame))
            console.log("********************************\n");
        var definitions = [];
        switch (choice) {
            case "1":
                if (!(isGame))
                    console.log("Some Definition texts of " + word + "\n");
                parsedBody.forEach(function(definition, index) {
                    if (isGame)
                        definitions.push(definition["text"]);
                    else
                        console.log(index + 1 + ". " + definition["text"] + "\n");
                });
                if (isGame)
                    resultArrObj["definition"] = definitions;
                break;
            case "2":
                if (isGame) {
                    resultArrObj["synonym"] = methods.printRelatedWords(parsedBody, "synonym", word, isGame);
                } else methods.printRelatedWords(parsedBody, "synonym", word, isGame);

                break;
            case "3":
                if (isGame) {
                    resultArrObj["antonym"] = methods.printRelatedWords(parsedBody, "antonym", word, isGame);
                } else methods.printRelatedWords(parsedBody, "antonym", word, isGame);
                break;
            case "4":
                console.log("Some of the examples of " + word + "\n");
                var examplesArray = parsedBody["examples"];
                if (Array.isArray(examplesArray)) {
                    examplesArray.forEach(function(example, index) {
                        console.log(index + 1 + ". " + example["text"] + "\n");
                    });
                }
                break;
            case "6":
                processParsedBody("1", parsedBody["word"], parsedBody["definitions"]); //Printing all definitions of the word of the day
                async.eachSeries(["2", "3"], function(choice, cb) {
                    requestHandler(choice, parsedBody["word"], cb);
                }, function(err) {
                    if (err) throw err;
                    processParsedBody("4", parsedBody["word"], parsedBody); //Printing all examples of the word of the day
                    inputProcessor();
                })



        }
    },
    printRelatedWords: function(relatedObjArray, relationType, word, isGame) {
        var notFound = true;
        for (var relatedObject of relatedObjArray) {
            if (relatedObject["relationshipType"] == relationType) {
                notFound = false;
                if (isGame) {
                    return relatedObject["words"];
                } else {
                    console.log("Some " + relationType + "s of " + word + "\n");
                    relatedObject["words"].forEach(function(word, index) {
                        console.log(index + 1 + ". " + word);//printing synonyms/antonyms
                    });
                }
            }
        }
        if (notFound && !(isGame))
            console.log("No such " + relationType + "s are there");

    },
    removeFirstElement: function(array) {
        array = array.splice(1);
        return array;
    }
}
module.exports = methods;