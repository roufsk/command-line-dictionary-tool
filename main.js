var prompt = require('prompt'),
    request = require('request'),
    async = require('async'),
    config = require('./config.js'),
    utilMethods = require('./utils.js'),
    getUrl = utilMethods.getUrl,
    processParsedBody = utilMethods.processParsedBody,
    getRandomArbitrary = utilMethods.getRandomArbitrary;
prompt.start();
var prevInputWord, limit = 20,
    synonym = true,
    schema = config.schema,
    apiKey = config.apiKey;
//adding shuffle method on string prototype 
String.prototype.shuffle = function() {
    var a = this.split(""),
        n = a.length;

    for (var index = n - 1; index > 0; index--) {
        var randomIndex = Math.floor(Math.random() * (index + 1));
        var tmp = a[index];
        a[index] = a[randomIndex];
        a[randomIndex] = tmp;
    }
    return a.join("");
}
//to handle the requests
function requestHandler(choice, word, cb, isGame) {
    request(getUrl(choice, word, apiKey, limit), function(err, response, body) {
        if (body == null || Array.isArray(body) && body.length == 0)
            console.log("result is empty");
        else {
            if (isGame) {
                processParsedBody(choice, word, JSON.parse(body), isGame);//parsing the body with respect to game
            } else
                processParsedBody(choice, word, JSON.parse(body), isGame);
        }
        if (cb)
            cb();
    });
}

function displayHint(resultArrObj, word, hint) {
    var hintDispalyed = false;
    Object.keys(resultArrObj).forEach(function(itm) {
        if (resultArrObj[itm] != undefined && resultArrObj[itm][0] != undefined && !(hintDispalyed)) {
            console.log("The " + itm + " of the word is:\n");
            console.log("\t" + resultArrObj[itm][0] + "\n");
            resultArrObj[itm] = utilMethods.removeFirstElement(resultArrObj[itm]);//removing every first element so that next it wont be repeted
            if (hint)
                hintDispalyed = true;//to enable only one hint after first hint
        }
    });
    if (!(hintDispalyed) && hint) {
        console.log("The shuffled word of the word is:");
        console.log(word.shuffle());//shuffle the characrers of the word
        hintDispalyed = true;
    }
}

function answerProcessor(resultArrObj, hint, synonymsArr, word) {
    if (!(hint)) {
        console.log("Guess the word--->\n");
        displayHint(resultArrObj, word, hint);
    }else{
        console.log("A.Try Again \nB.Hint \nC.Quit");
    }
    //getting the input word from the user
    prompt.get(schema.inputWord, function(err, result) {
        if (result.word == word) {
            console.log("Hurray you won the game");
            inputProcessor();
            return;
        } else if (synonymsArr) {
            synonymsArr.forEach(function(synonym) {
                if (result.word == synonym) {
                    console.log("Hurray you won the game");
                    inputProcessor();
                    return;
                }
            });
        }
        
            if (result.word == "A")
                answerProcessor(resultArrObj, true, synonymsArr, word);
            else if (result.word == "B") {
                answerProcessor(resultArrObj, false, synonymsArr, word);
            } else if (result.word == "C") {
                async.each(["1", "2", "3", "4"], function(choice, cb) {
                    requestHandler(choice, word, cb);
                }, function(err) {
                    if (!err)
                        inputProcessor();
                })
            }else{
                answerProcessor(resultArrObj, true, synonymsArr, word);
            }

    });
}

function playGame(choice) {
    resultArrObj = {};
    request(getUrl(choice, null, apiKey), function(err, response, body) {
        if (err) throw err;
        var word = JSON.parse(body)["word"];
        async.each(["1", "2", "3"], function(opt, cb) {
            requestHandler(opt, word, cb, true);
        }, function(err) {
            if (err) throw err;
            console.log("********************************\n");
            if (resultArrObj["synonym"])
                var synonymsArr = resultArrObj["synonym"].slice(0);
            answerProcessor(resultArrObj, false, synonymsArr, word);
            // inputProcessor();
        })

    });
}



function getWordAndProcess(choiceObj) {
    prompt.get(schema.inputWord, function(err, wordObj) {
        if (err) throw err;
        if (!(wordObj.word == "y"))
            prevInputWord = wordObj.word.toLowerCase();
        if (choiceObj.choice == "5") {
            async.each(["1", "2", "3", "4"], function(choice, cb) {
                requestHandler(choice, prevInputWord, cb);
            }, function(err) {
                if (!err)
                    inputProcessor();
            })
        } else {
            requestHandler(choiceObj.choice, prevInputWord, inputProcessor);
        }

    })
}

function inputProcessor() {
    console.log("********************************\n");

    console.log("1. Word Definitions \n2. Word Synonyms\n3. Word Antonyms\n4. Word Examples\n5. Word Full Dict\n6. Word of the Day Full Dict\n7. Word Game");

    prompt.get(schema.inputChoice, function(err, choiceObj) {
        if (err) throw err;
        if (choiceObj.choice == "6") {
            prompt.get(schema.dateFormat, function(err, dateObj) {
                requestHandler(choice, prevInputWord, cb);
            });
        } else if (choiceObj.choice == "7") {
            playGame("7");
        } else {
            if (prevInputWord)
                console.log("To use previous word as the input then enter y");
            getWordAndProcess(choiceObj);

        }


    });
}
inputProcessor();