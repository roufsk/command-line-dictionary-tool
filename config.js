 var data = {
     schema: {
         dateFormat: {
             properties: {
                 date: {
                     description: 'Enter a date in format YYYY-MM-DD(2018-09-22)',
                     pattern: /^(19|20)\d\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$/,
                     message: 'Date must be in YYYY-MM-DD format',
                     required: true
                 }
             }
         },
         inputWord: {
             properties: {
                 word: {
                    description: 'Enter a word it should contain only alphabets',
                     pattern: /^[a-zA-Z]+$/,
                     message: 'Word must contain only alphabets',
                     required: true
                 }
             }
         },
         inputChoice: {
             properties: {
                 choice: {
                     description: 'Choose an option between 1 -7',
                     pattern: /^[1-7]+$/,
                     message: 'Choice should be between 1-7',
                     required: true
                 }
             }
         }

     },
     apiKey: "&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5"

 }
 module.exports = data;